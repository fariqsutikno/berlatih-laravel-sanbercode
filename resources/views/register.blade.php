<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook - Form Sign Up</title>
</head>

<body>
    <a href="index.html">Kembali ke Home</a>
    <h1>Create New Account - SanberBook</h1>

    <form action="/home" method="post">
        @csrf
        <fieldset>
            <legend>
                <h3>Sign Up Form</h3>
            </legend>

            <!-- [First Name] -->
            <label for="firstname">First name:</label><br><input type="text" name="firstname" id="firstname"><br><br>
            <!-- Last Name -->
            <label for="lastname">Last name:</label><br><input type="text" name="lastname" id="lastname"><br><br>
            <!-- Gender -->
            <label>Gender: </label><br>
            <input type="radio" name="gender" id="male" value="male"><label for="male">Male</label><br>
            <input type="radio" name="gender" id="female" value="female"><label for="female">Female</label><br>
            <input type="radio" name="gender" id="other" value="other"><label for="other">Other</label><br><br>
            <!-- Nationality -->
            <label for="nationality">Nationality:</label><br>
            <select name="nationality" id="nationality">
                <optgroup label="Asia">
                    <option value="indonesian">Indonesian</option>
                    <option value="malaysian">Malaysian</option>
                    <option value="singapore">Singapore</option>
                    <option value="brunnei">Brunnei</option>
                </optgroup>
                <optgroup label="Europe">
                    <option value="france">France</option>
                    <option value="english">English</option>
                    <option value="germany">Germany</option>
                    <option value="swiss">Swiss</option>
                </optgroup>
            </select><br><br>
            <!-- Language Spoken -->
            <label>Language Spoken</label><br>
            <input type="checkbox" name="lang_spoken" id="indonesian" value="indonesian"><label
                for="indonesian">Indonesian</label><br>
            <input type="checkbox" name="lang_spoken" id="english" value="english"><label
                for="english">English</label><br>
            <input type="checkbox" name="lang_spoken" id="other" value="other"><label for="other">Other</label><br><br>
            <!-- Bio -->
            <label for="bio">Bio</label> <br>
            <textarea name="bio" id="bio" cols="30" rows="10">Perkenalkan nama saya ...</textarea><br><br>
            <!-- Submit Button -->
            <input type="reset" value="Reset">
            <input type="submit" value="Sign Up">
        </fieldset>
    </form>
    <p>&copy 2021 - PT. SanberBook Indonesia</p>
</body>

</html>