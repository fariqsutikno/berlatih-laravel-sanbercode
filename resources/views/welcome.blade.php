<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook - Selamat Datang di Halaman Dasbor</title>
</head>

<body>
    <h1>Selamat Datang, wahai {{$user['firstname'] . " " . $user['lastname']}}!</h1>
    <h3>Terima kasih telah bergabung di SanberBook. Social media kita bersama.</h3>
    <p>&copy 2021 - PT. SanberBook Indonesia</p>
</body>

</html>